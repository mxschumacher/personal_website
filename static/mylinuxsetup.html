<html>

<head>
  <title>linux setup</title>
  <link rel="icon" href="data:,">
  <link href="style.css" rel="stylesheet" type="text/css" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>
  <h1>A productivity-centric desktop linux setup</h1>
  <p>By Max Schumacher (<a href="https://twitter.com/mxschumacher">mxschumacher</a>)</p>
  </p>

  <p>I enjoy reading about the development machines of others
    and want to share my current desktop setup below. There is a risk of
    procrastinating on actual software engineering tasks by fine-tuning a
    desktop configuration, but by <s>stealing</s> drawing
    inspiration from others, you can get a highly customized setup without
    too much work. What follows isn't the result of an afternoon of
    tinkering, but rather a <a href="https://news.ycombinator.com/item?id=19256391">slow
      evolution over years</a>. This process is not and probably never will be "done",
    new challenges and opportunities come along and I will adapt
    accordingly.
  </p>
  <p>Friends and family sometimes ask me to fix some
    software problem for them. I then get to use their personal computers, which often take
    forever to perform the simplest tasks. You want to open a file explorer? That's
    going to take several seconds. Rebooting a machine? Make yourself a coffee and come back later.
    That is a long, long time for modern day hardware and I am not willing to wait.
    "We're going to need 45 minutes to update your system" is not
    acceptable to me.</p>
  Given how powerful hardware has become, I want all interaction to be close to instant.</p>

  <p>My goal is to have a machine that enables me to be
    productive. I'm looking for a short path between what I have in mind and
    what I can make appear on the screen. A tight feedback loop between mind and machine. I find this requirement
    to be essential for productivity.
    Common tasks like launching a terminal, switching to another workspace or invoking certain commands in
    my shell should be easy and fast. <a
      href="https://web.archive.org/web/20210416001735/https://linuxrig.com/2019/09/18/the-linux-setup-drew-devault-developer-and-maintainer/">Drew
      DeVault</a> put it well when he said "It's
    what so many of us are looking for: a frictionless experience that
    instantly moves thoughts and ideas from our brain to the screen.".
    Common command line utilities can be invoked via <a
      href="https://github.com/mxschumacher/machine_setup/blob/master/dotfiles/config.fish">short
      aliases</a>and fish's based <a
      href="https://fishshell.com/docs/current/index.html#autosuggestions">auto-completion</a>
    are great helpers.</p>
  <p>I try not to be dogmatic, I neither believe
    that CLIs are always better than GUIs, nor do I think it is useful to
    aim for goals like "you should never have to take your hands off the
    keyboard". For tasks like moving files, using git (especially when
    resolving larger merge conflicts), interacting with databases or
    changing system settings I have found GUIs to be faster and more convenient than terminal based approaches.</p>

  <p>Generally, I don't want to be interrupted by
    notifications about software updates or emails. Instead of "push
    notifications", I prefer to "pull" my emails/messages when I'm not
    currently concentrating on another task.

  <h3>Hardware</h3>

  <p>My laptop is a <a href="https://www.notebookcheck.net/Lenovo-ThinkPad-T450s-Ultrabook-Review.137248.0.html">Lenovo
      T450s</a>with an Intel i7-5600U CPU,
    12GB of RAM and an Nvidia GeForce 940m, it's about 5 years old now. The
    machine has been through lots of travel and at least one fall. The
    batteries aren't what they used to be and my Enter key recently fell
    off, but the laptop is excellent overall. As of now, I don't use an
    external screen or keyboard, but occasionally work with a mouse. I'm
    playing with the thought of buying a more powerful machine, but I doubt
    it would make me a more productive programmer. Laptops with
    significantly more than 16GB of RAM still seem to be rare. Instead of
    upgrading my hardware, I'm more interested in figuring out how to
    flexibly shift larger workloads into the cloud. After <a
      href="https://news.ycombinator.com/item?id=28606962">glowing reviews on HN</a>, I am
    keen to make the <a href="https://frame.work/fr/en/laptop-diy-12-gen-intel">Framework laptop my next machine</a>.
    <img src="images/image7.jpg">

  <h3>Operating System / Distro</h3>

  <p>The basis for development machine setup is <a href="https://manjaro.org">Manjaro Linux</a>,
    a pre-configured version of Arch Linux with a GUI that is easy to
    install. There's no need to bother with disk partitions or networking
    drivers: it just works. Instead of slowly building the system I want, I
    gradually remove everything I don't need. A purist approach would have
    been to start with Arch directly, but I don't really see why I should
    spend the time on a <a href="https://wiki.archlinux.org/index.php/installation_guide">fresh
      Arch install</a>if I can get sensible
    defaults in seconds.</p>

  <p>This is what <a href="https://github.com/dylanaraps/neofetch">neofetch</a>has to say:</p>

  <img src="images/image3.png">

  <p>The reasons for choosing an Arch based distro are
    simple: the <a href="https://wiki.archlinux.org">documentation
      is outstanding</a>and the <a href="https://aur.archlinux.org">packages available to Arch</a>are
    expansive and up to date. Whenever I hear of any
    new tool on Hacker News, Twitter or Github, there is a good chance I can install the most recent version
    with a single command. No manual compilations, no outdated versions, no
    dependency conflicts. I cannot stress enough how great this is.</p>
  </p>
  <p>At some point my last Ubuntu desktop forced me to go
    through a major version update, otherwise I wouldn't be able to get
    additional package updates. To avoid a similar situation in the future,
    I have embraced the <a href="https://en.wikipedia.org/wiki/Rolling_release">rolling
      release model</a>of Arch. A nice perk is to have
    easy access to recent Linux kernels, the <a
      href="https://cdn.kernel.org/pub/linux/kernel/v5.x/ChangeLog-5.18.7">most recent stable release is 5.18.7</a>,
    and 5.19.0 is running on my machine right now - not too
    shabby!</p>
  <p>
  </p>
  <p>To block malicious hosts and
    social media sites, I modify the /etc/hosts files on my machine with the
    <a href="https://github.com/StevenBlack/hosts">hosts</a>, as their documentation aptly puts it "Having a smart hosts
    file goes a long way towards blocking malware, adware, and other
    irritants." I do slightly modify the list though to enable Whatsapp - which is not a distraction for me.
  </p>

  <p>
    <img src="images/installingArchLinux.jpg">
  </p>

  <p>
    For window management, I use <a href="https://i3wm.org">i3-wm</a>,
    I have not made the jump to the Wayland-based <a href="https://swaywm.org">sway</a>, mostly
    because I've never had any problems with i3 or the x window system. The window manager allows
    me to make efficient use of my screen "real estate": no more unused
    areas of overlapping windows. In addition, work spaces can be easily set
    up and allow me to quickly jump from my IDE, to my browser, to my shell
    and back. i3 provides some displays about the system status (networking,
    RAM utilization etc), but the default display is a bit raw. Luckily, <a
      href="https://github.com/polybar/polybar">polybar</a>makes it easy to have beautiful and minimalist status
    bars in i3.</p>
  </p>
  <img src="images/image9.png">
  </p>
  <p>As an application launcher, I have set up <a href="https://github.com/davatorium/rofi">rofi</a>, it is accessible
    via a hotkey and much nicer than the
    default dmenu, because it sorts applications according to frequency of
    usage.</p>
  </p>
  <p><img src="images/image2.png">
  </p>
  <p>The terminal emulator I use is <a href="https://github.com/alacritty/alacritty">Alacritty</a>,
    the shell is <a href="https://fishshell.com">fish</a>,
    with <a href="https://stackoverflow.com/questions/28444740/how-to-use-vi-mode-in-fish-shell">Vim
      bindings</a>enabled. If I type in a long
    command and want to make modifications, I can easily jump to the right
    spot instead of holding the arrow key for several seconds; I've been
    looking for such a feature for a long time! </p>
  </p>
  <p>To handle extensions of my shell, I use omf (<a href="https://github.com/oh-my-fish/oh-my-fish">oh-my-fish</a>),
    the theme I've been using for years and am very happy with is called <a
      href="https://github.com/umayr/theme-sushi">sushi</a>.

    Some people complain about a lack of complete POSIX compliance in fish
    that breaks some commands: other than minor modifications I've never had
    any trouble. More <a href="https://news.ycombinator.com/item?id=15911139">complex
      scripts are being run in bash anyway</a>, so
    occasionally changing a quotation mark or escaping some character from
    the code I blindly copy from Stack Overflow isn't a big deal for me.</p>
  </p>
  <p><img src="images/image6.png"> </p>
  </p>
  <p>To access the Arch packages on AUR, I use <a href="https://github.com/Jguer/yay">yay</a>.
  </p>
  <h3>Data protection</h3>
  </p>
  <p>There is no automatic remote backup of my system right
    now, just one folder with files that I don't want to lose that sync to <a href="https://cozy.io/en">Cozy</a>.
    I manually export data from <a href="https://workflowy.com">Workflowy</a>,
    OneTab and my Google account and Twitter into this folder. In the past,
    Linux clients from Dropbox have caused performance problems for me, but
    now I'm giving a new project called <a href="https://github.com/SamSchott/maestral">maestral</a>a
    try to see whether Dropbox on a Linux desktop is feasible again. Code
    and system configurations are usually on remote git repositories, either
    on Github or <a href="https://gitlab.com/mxschumacher">Gitlab</a>. I occasionally try to put a backup on an external
    hard
    disk. There's definitely room for improvement through automation here.
    My aim is to always be protected against loss of data, be it through
    theft, destruction or otherwise losing access to my laptop. If the house
    burns down right now, that should not mean loss of data for me. I should
    get much more aggressive about redundantly storing my data and
    encrypting it.</p>
  </p>
  <h3>Modern replacements for standard utilities.</h3>
  </p>
  <p>grep, ls, cat, fd and others are familiar tools that
    many programmers use on a daily basis. Modern, expanded implementations,
    <a href="https://medium.com/paritytech/why-rust-846fd3320d3f">often
      in Rust</a>, are available. To avoid having to change my
    habits, I use the new utilities though the names of the old commands by
    way of aliases. My dot files are open source and contain some nice shortcuts, please have a look: <a
      href="https://github.com/mxschumacher/machine_setup/blob/89df00668e2cc3a4366bdfecb22b2ea919ee7420/dotfiles/config.fish"></a>.
  </p>

  </p>
  <p>To see the contents of files, it is often convenient to
    run cat $filename, I've recently switched to <a href="https://github.com/sharkdp/bat">bat</a>,
    a similar command with convenient syntax highlighting:</p>
  </p>
  <p><img src="images/image5.png"></p>
  </p>
  <p>Instead of the default grep, I use <a href="https://github.com/BurntSushi/ripgrep">ripgrep</a>.
    Grep is useful to find lines in large codebases, it is
    especially handy when I have the source of my dependencies/components on
    the same machine and can quickly find the code that triggers an error
    message, it is a bit crazy how fast the ripgrep implementation is. The <a
      href="https://blog.codinghorror.com/learn-to-read-the-source-luke">code
      is the source of truth</a>and it can be easier to
    look at the source than to aimlessly search for error messages in old
    threads. Grep or <a
      href="https://ia802309.us.archive.org/25/items/pdfy-MgN0H1joIoDVoIC7/The_AWK_Programming_Language.pdf">AWK</a>also
    make it easier to parse large logs. Instead, of ls I use <a href="https://github.com/Peltoche/lsd">lsd</a>,
    <a href="https://github.com/sharkdp/fd">fd</a>is
    a modern incarnation of find.
  </p>
  </p>
  <h3>Applications</h3>
  </p>
  <p>Firefox is the browser I use, the extensions are <a href="https://www.eff.org/https-everywhere">HTTPs
      everywhere</a>, <a href="https://github.com/gorhill/uBlock">uBlock
      Origin</a>and <a href="https://www.one-tab.com">oneTab</a> and bitwarden.

    OneTab is particularly important to me, because it allows me to
    store groups of tabs as collections associated with a certain theme or
    task: If I want to read an article later or pick up a task at another
    point in time, i just throw it into OneTab, this frees up a lot of
    system resources; some websites occupy an outrageous amount of memory. I
    wish I had a tool like OneTab for desktop apps: taking a snapshot of the
    current state of my IDE, browser and servers and just being able to
    close it in order to work on another project.</p>
  </p>
  <p>Now that Gmail has started to display ads directly in
    the inbox, I'm getting ready to pay for an email provider; I'd also like
    to get away from using a web app, to havinga
    local copy of all my emails. I hear good things about <a href="https://getmailspring.com">Mailspring</a>as
    an email client, but I'm worried about losing the extremely powerful
    search functionality Gmail has to offer. I have an account with <a href="https://protonmail.com">Protonmail</a>,
    but am also considering Fastmail. Leaving Gmail is a tedious and manual
    process, I doubt I'll ever be able to delete my account.</p>
  </p>
  <p>I also have the Firefox <a href="https://www.mozilla.org/en-US/firefox/developer">Developer
      Edition</a>and a Google Chrome
    installation without any extensions that could interfere with the web
    application I'm working on. The idea here is to see a web app the way a
    user would.</p>
  </p>

  <p>As a password manager, I am using the excellent <a href="https://bitwarden.com">Bitwarden</a>, I
    have the browser extension within Firefox and the mobile app on my phone. I use the password
    manager in conjunction with the <a href="https://github.com/andOTP/andOTP">andOTP</a>app (a one time password
    provider) on my Android phone and a <a href="https://www.yubico.com/why-yubico/for-individuals">Yubikey</a>. I have
    tried to
    migrate away from any SMS based 2FA but
    given the hundreds accounts I have with various services, it is hard to be
    sure which accounts still use my phone number as backup.</p>
  </p>

  <p>In regards to developer tooling, I typically switch between the various Jetbrains IDEs and Visual Studio code,
    always depending on the task at hand
    and <a href="https://github.com/mxschumacher/machine_setup/blob/master/pycharm_settings/installed.txt">several
      plugins</a>, The Jetbrains IDEs like Pycharm or Goland are great: the
    debugger is powerful and convient; functionality like "Find usages", "Refactor", "Go
    to definition" and the git-GUI are highly appreciated. By using Vim
    bindings within a Jetbrains IDE, I feel like I'm getting the <a
      href="https://news.ycombinator.com/item?id=21343089">best
      of both worlds</a>without having to put
    a lot of work into setup. I also use Visual Studio code for smaller
    projects or quick edits, Jetbrains tools are pretty heavy.</p>
  </p>
  <p>To run queries against my web servers during the
    development, I rely on <a href="https://httpie.org">httpie</a> for a CLI-based approach or Insomnia when I want to
    use a GUI.
    To download videos I like on Youtube or <a href="https://vimeo.com/mxschumacher/likes">Vimeo</a>, I
    use <a href="https://github.com/ytdl-org/youtube-dl">youtube-dl</a>.
  </p>
  </p>
  <p>A desktop system needs to be <a href="https://wiki.archlinux.org/index.php/System_maintenance">maintained</a>:
    install software updates for the various packages, clear log files and
    the trash, check for orphaned packages, delete binaries I don't use
    anymore, upgrade my kernel, run a virus scanner (I use <a href="https://www.clamav.net">clamav</a> to
    scan my home folder for known malware signatures), investigate failed
    processes, check what is on the file system etc. </p>
  </p>
  <p><a href="https://utils.kde.org/projects/filelight">Filelight</a> is
    a great application to visualize and find out what is happening on a
    file system. I'm often surprised to learn that some software I have
    deleted long ago still occupies a good chunk of my hard drive or that
    some old cache weighs in at 10GB+.<br>
    <br>
    <img src="images/image1.png">
  </p>
  </p>
  <p>I have tried to automate several of these steps in <a
      href="https://github.com/mxschumacher/machine_setup/blob/master/scripts/system_maintenance">a
      small script</a>, but this is very much a work in process
    and I'm sure many of the more experienced Unix users want to tear their
    hair out when looking at it. A much <a href="https://gitlab.com/mgdobachesky/ArchSystemMaintenance">higher
      degree of sophistication</a> is possible here.</p>
  </p>
  <p>To access files, I use <a href="https://krusader.org">krusader</a>,
    which allows me to open two directories at the same time and remembers
    the last opened directory. This is handy for moving files/folders
    manually. </p>
  </p>
  <p>To view PDFs, I use Firefox, which has the nice benefit
    of letting me open web-links within documents in new tabs (instead of
    having to switch to a browser in another window). To read ebooks I use <a
      href="https://github.com/johnfactotum/foliate">foliate</a>,
    I play back videos with <a href="https://www.videolan.org/vlc">vlc</a>, but I guess Firefox should also be able to
    do that, too.</p>
  </p>
  <p>To take screenshots, I use <a href="https://github.com/lupoDharkael/flameshot">flameshot</a> which I invoke
    through a convenient shortcut. It is nice to be able to insert little arrows into screenshots and then send
    them off. Flameshot allows for screenshots with a slight delay, which is
    handy when trying to capture a part of a UI that results from a
    mouseover event.</p>
  </p>
  <p>As a task manager, I use <a href="https://github.com/aristocratos/btop">btop</a>,
    it is great to identify rogue processes that gobble up all system
    resources and snipe them down. F4 to filter, F9 + 9 to kill. It's a bit
    scary how often I only find out about a problematic process like this
    because the fans of my laptop flare up. It's a bit unfortunate that I
    cannot hear the fans of the machines running my cloud applications.</p>
  </p>
  <img src="https://static.haydenjames.io/wp-content/uploads/2021/11/btop.png">
  </p>
  <p>For natural language writing, I switch between a text
    editor like VSCode, Libreoffice Write and Google Docs. I would love to
    have an integration into the <a href="http://www.hemingwayapp.com">Hemingway
      app</a>to improve my style. It is hard
    to find the right balance here: Google Docs isn't available when I'm
    offline but Libreoffice is much worse for collaboration and doesn't have
    the same strength in suggesting improvements to my text. The beauty of
    writing in plain text is that I don't have to worry about binary formats
    ever being inaccessible. The rest of the LibreOffice suite is installed
    on my machine, but I don't have much use for it.</p>
  </p>
  <p>Especially when in public or at an office, it is a good
    idea to lock one's screen, I let the alias "afk" trigger <a
      href="https://github.com/meskarune/i3lock-fancy">i3lock-fancy</a>, which blurs and locks my screen.<br>
  </p>
  <p>While I have not done as many screencasts as I would
    like, tools that have been recommended to me for this purpose are <a
      href="https://github.com/obsproject/obs-studio"> Obs
      Studio </a> and <a href="https://www.thregr.org/%7Ewavexx/software/screenkey"> Screenkey </a>.
  </p>
  <p><br>
  <p>Using the "man" command to show the manual
    of an application can be intimidating and unhelpful - walls of text for
    something that should be pretty simple. An modern interpretation of the
    man pages is <a href="https://github.com/tldr-pages/tldr">tldr</a>,
    which shows succinct summaries of what a command can do and gives
    practical, real-world examples<br>
  </p>
  <p>To interact with databases, I currently use <a href="https://dbeaver.io">Dbeaver</a>, just because I am too cheap
    to pay for Datagrip. psql should suffice anyway.</p>
  </p>
  <img src="https://dbeaver.io/wp-content/uploads/2018/03/data_edit.png">


  <p><a href="https://workflowy.com">Workflowy</a> serves as my notebook and to do list, a good part of my
    life is stored there.</p>
  </p>
  <p>Now that I live in France without being able to
    correctly write in French, the <a href="https://www.deepl.com/translator">translation
      tool Deepl</a> has become really
    important to me.</p>
  </p>
  <p>To get an overview of the Docker images and containers
    on my system, I find that <a src="https://docs.portainer.io/start/install/server/docker/linux">Portainer</a> is a
    nice solution.
    <img
      src="https://2.bp.blogspot.com/-N7piChMnv4Y/XNv01GHPOlI/AAAAAAAACw4/x41sXFftxpAFin_tsFLH7v-WwAZbS-nwACLcBGAs/w1200-h630-p-k-no-nu/portainer.png">
  </p>
  <p>Now that I have read <a href="https://www.gwern.net/Spaced-repetition">so many good things</a>about <a
      href="http://augmentingcognition.com/ltm.html">spaced
      repetition</a>, I try to insert newfound knowledge into <a href="https://apps.ankiweb.net">Anki</a>. Insights
    about French, software engineering, investing or
    myself go straight into the app.
  </p>

  <p>For my private communication, I mostly rely on Whatsapp,
    which I use via their web app , but I also have
    the desktop app of <a href="https://telegram.org">Telegram</a>(very
    fluent experience!).
  </p>
  <p>I don't play games anymore so far, I have not felt the need to use Windows apps via
    wine.
  </p>
  <p>The software I'm using is almost entirely
    open source and it is incredible to me that so many talented engineers
    from around the world have worked hard to make those tools stable,
    useful and fast. If you also consider the makers of the dependencies
    (and their dependencies …) and build tools that go into each project,
    we're talking about a distributed army of passionate contributors. One day, I'd
    love to visualize the dependency graph and map commits to contributors.</p>
  </p>
  <h3>What am I missing, what are the next few
    issues I want to tackle?</h3>
  </p>
  <p>A global system settings GUI for things like
    default app associations for a given file format, system fonts, network settings
    would be great. I currently either modify text files or try to make my
    way through disparate GUIs to deal with things like Bluetooth, GPU
    drivers or WiFi.</p> Each utility and desktop component I use has a slightly different
  approach to configuration and it is a tedious, web-search heavy approach to make any kind of progress.
  </p>
  <p>Maybe it makes sense for me to run a VPN, I have to
    investigate that further, plenty of people sing the praise of <a href="https://www.wireguard.com">WireGuard</a>as a
    client.
  </p>
  </p>. Tailscale is another widely celebrated layer built on top of the aforementioned WireGuard that is worth
  exploring.
  <p>To learn and customize more, I do plan to eventually run
    Arch on <a href="https://en.wikipedia.org/wiki/Coreboot">coreboot</a>with
    a custom kernel I compile myself <a href="https://lwn.net/Articles/734071">using clang</a>,
    maybe even on top of hardware I've selected myself. The experience of going
    through <a href="https://www.linuxfromscratch.org/">Linux from Scratch </a> is also widely recommended. My attempts
    of doing
    so within <a href="https://www.qemu.org">qemu</a> have not borne fruit so far. Ideally I'd like to aim
    for a fully automated and reproducible process. Starting from a minimal
    install would certainly help in cutting down the number of installed
    packages on the system, thus decreasing the overall complexity of the system.</p>
  </p>
  <p>Here's a list of similar posts where other
    developers present their systems and design goals:
  </p>
  <ul>
    <li><a href="https://begriffs.com/posts/2017-05-17-openbsd-workstation-guide.html">
        https://begriffs.com/posts/2017-05-17-openbsd-workstation-guide.html</a>
    </li>

    <li><a href="https://www.andreafortuna.org/2017/07/14/ultra-geek-linux-laptop-my-own-setup">
        https://www.andreafortuna.org/2017/07/14/ultra-geek-linux-laptop-my-own-setup/</a>
    </li>

    <li><a href="https://code.krister.ee/my-coding-environment">https://code.krister.ee/my-coding-environment/</a>

    <li><a href="https://hookrace.net/blog/linux-desktop-setup">https://hookrace.net/blog/linux-desktop-setup/</a>

    <li> <a href="https://medium.com/@jonyeezs/my-minimal-over-powered-linux-setup-guide-710931efb75b">
        https://medium.com/@jonyeezs/my-minimal-over-powered-linux-setup-guide-710931efb75b</a>
    </li>

    <li><a href="https://github.com/brpaz/my-linux-setup">https://github.com/brpaz/my-linux-setup</a>
    </li>
    <li><a
        href="https://blog.burntsushi.net/system76-darter-archlinux">https://blog.burntsushi.net/system76-darter-archlinux/</a>
    </li>
    <li><a href="https://linuxrig.com/2019/12/04/the-linux-setup-kezz-bracey-web-designer-developer">
        https://linuxrig.com/2019/12/04/the-linux-setup-kezz-bracey-web-designer-developer/</a>
    </li>
  </ul>
</body>

</html>