---
title: About me
comments: false
---

This website is an attempt at tying together my activities on various websites, I frequently comment on [Hacker News](https://news.ycombinator.com/threads?id=mxschumacher), [Seeking Alpha](https://seekingalpha.com/user/28152333/comments) and [Twitter](https://twitter.com/mxschumacher). Sometimes I write on [Reddit](https://www.reddit.com/user/mxschumacher/).

My childhood was spent in in the countryside of Hessen, Germany. As an exchange student and later on a journey to get to knoy Silicon Valley, I spent more than a year in the United States and have now been living in Paris for several years.

A lot of my attention goes towards investing and programming.

I taught myself to program, the initial impetus was to attain the ability to realise my own long
list of project ideas. Without software engineering ability, it is hard to to think clearly about what the implementation of a given concept would entail. Learning to code is a continuous journey of learning and empowerment. Having one's thought be processed by a compiler
is also frequently a humility inducing activity.

Reading is one of the great passions of my life, to get an idea of the kinds of books I'm interested in, please have a look at my [Goodreads](https://www.goodreads.com/review/list/10152457) page.

Taking pictures brings me joy, I have published some of them on [EyeEM](https://www.eyeem.com/u/mxschumacher).
