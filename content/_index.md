# Welcome

Some of my recent essays can be found here:

[An introduction to investing](https://mxschumacher.xyz/investing.html)

[My Linux setup](https://mxschumacher.xyz/mylinuxsetup.html)

[Is food fuel?](https://mxschumacher.xyz/isfoodfuel)
