---
title: "Long oil & gas"
date: 2019-10-03
tags: ["investing", "oil"]
---

With the rise of electric and hybrid vehicles, a global political movement to combat climate change (Hi, Greta!) and [strong growth in renewable energy](https://www.stripes.com/news/us/a-mix-of-solar-wind-and-batteries-threatens-the-future-of-nuclear-power-1.600949) it's easy to think that the demise of the oil and gas industry is imminent.


Oil is the most important natural resource in the history of the world — no sector is more entrenched in the global value chain and the political system. Its cheap and abundant availability is a necessary condition for most personal mobility, logistics, electricity production, heating, fertilizers, packaging and various industrial processes.

The market capitalisation of oil companies is, to a large extend a function of the future expectations of what the oil price will be and how much oil will be consumed. Higher prices lead to higher earnings which lead to higher valuations, the inverse also holds. To get an understanding of future price movements, let's observe both supply and demand.

I will provide many arguments for why I believe it is reasonable to assume that the demand of oil in the world will be substantial and continue to grow in the coming years.

oil is a commodity, meaning that every barrel of oil is considered to be equivalent. For the finished product, there is no differentiation. Commodity products by definition have weaker margins, because of enormous competitive pressures of hundreds of large companies producing the same thing. Selling a commodity stands in stark contrast to holding a technological patent for a product that is in high demand or having a strong brand (like selling Ferraris, Luis-Vuitton handbags or Big Macs). To be profitable, firms use very large economies of scale and aggressive cost optimization.

### Demand

Petroleum is an incredibly versatile raw ingredient: unless we dramatically change packaging, fertilization, pharmaceuticals, cosmetics and other uses of plastics, oil demand will be high for years. In his book [Making the Modern World: Materials and Dematerialization]([https://www.goodreads.com/book/show/17941760-making-the-modern-world](https://www.goodreads.com/book/show/17941760-making-the-modern-world)) Vaclav Smil points out that de-materialization (Cars are getting lighter and entire categories of products are replaced by smartphones, lots of things can be delivered over the internet instead of having to be printed on paper and shipped) is happening, but humanity still devours an astonishing quantity of petrochemicals.

Closely related to oil is natural gas, it's often a byproduct to oil production
To [quote Australia's APPEA](https://www.appea.com.au/oil-gas-explained/benefits/oil-and-gas-in-everyday-life/):

> [Gas] is also important for fuelling many industrial operations,
> including glass and steel foundries, aluminium or nickel smelters, and
> many manufacturing industries. Gas is used in producing fertilisers
> and a wide range of industrial products, including plastics and
> polymers, textiles and paints and dyes."

For an overview of American petrol usage, the EIA provides [a good summary](https://www.eia.gov/energyexplained/oil-and-petroleum-products/use-of-oil.php)

The current installment of [**1.3bn cars**](https://wardsintelligence.informa.com/WI058630/World-Vehicle-Population-Rose-46-in-2016) on planet Earth is not just growing quickly, but will be around for a long time. About [80m cars are sold globally per year](https://www.statista.com/statistics/200002/international-car-sales-since-1990/). It's hard to estimate how many vehicles are decomissiond, because cars that are not longer wanted in wealthier nations often get transported to lower GDP countries to have a "second life". Some predictions say we will see 2bn+ internal combustion engine (ICE) cars within a couple of decades. [The average US car in 2018 consumes around 9.5l of fuel](https://www.reuters.com/article/us-autos-emissions/u-s-vehicle-fuel-economy-rises-to-record-24-7-mpg-epa-idUSKBN1F02BX) and the average American drives 21700km per year. That's almost 2100l of petrol per person per year, just for personal mobility by car (e.g. ignoring the kilometers traveled by the products we consume or when we fly, take boats, trains etc).

One could think that most people want to protect the environment and therefore purchase low-emissions cars like the one below

![Renault Twizy](https://www.autocar.co.uk/sites/autocar.co.uk/files/styles/gallery_slide/public/renault-twizy-1.jpg?itok=5wL5nLeh)

But this is rarely the case, the #1 [most sold car of 2018](https://www.best-selling-cars.com/global/2018-full-year-international-global-top-selling-car-models/) is he Ford F-150! Not exactly the most climate friendly mobility option, but people can't get enough of large trucks. More than one million of these machines have been sold in 2018 alone.

![More than one million of these have been sold in 2018! ](https://di-uploads-pod14.dealerinspire.com/kingsford/uploads/2018/02/ford_f-150-2018_blue.png)

Together, Dodge's RAM pickup and GM's Silverado, both comparable in size to the F150 above, contribute another ~1.2million sold pickups in the US in 2018.

There's an unprecedented shift towards SUVs and trucks in the car market. In Germany, more than 20% of new cars sold in 2019 are SUVs, dubbed "offroad city-cars". For the first time since 1995, the trend of falling fuel consumption in Germany has been broken. The average car on the road in 2017 uses [more fuel than in 2016](https://www.umweltbundesamt.de/daten/verkehr/kraftstoffe).

### the importance of oil

The willingness of national governments to spend trillions of dollars in military operations to secure oil supplies and to accept (or even support!) dictatorships that are guilty of human rights violations offers a glimpse into just how important crude really is. [Leif Wenar’s book “Blood Oil”]([https://www.goodreads.com/book/show/25735903-blood-oil](https://www.goodreads.com/book/show/25735903-blood-oil)) gives an grueling account of the current situation. Memories of the oil price shock and its devastating effects are deeply ingrained in the minds of politicians, consumers and voters. Growth, stability and prosperity are at risk if oil is not readily available.

![oil price shock 1979](https://www.thebalance.com/thmb/OWbuY6sEFNjheUSzugZGBpPxmPk=/3668x2479/filters:fill%28auto,1%29/GettyImages-544377756-5a8c9a6ca9d4f900368f9603.jpg)

A large parts of oil production goes into moving ships and airplanes. There are no fundamental technological changes in sight in these areas and these machines tend to be used for a long time, there will be a lot of demand for oil in the decades to come, even if 100% of cars are electric. As of now, [Boeing has an order backlog of **4415** 737 airplanes](http://investors.boeing.com/investors/fact-sheets/default.aspx) while that of [Airbus stands at **7172** jetliners.](https://www.airbus.com/aircraft/market/orders-deliveries.html)  Without a single new order they’ll still build planes at full capacity for years to come. I invite you to look into infrastructure spending for airports in China and observe how many airplane orders are coming in from India. A commercial space industry is developing in the US and China; the number of [rockets launched into space is growing quickly](https://en.wikipedia.org/wiki/Timeline_of_spaceflight).

[Over 51.000](https://www.statista.com/statistics/264024/number-of-merchant-ships-worldwide-by-type/) cargo ships are cruising the world’s oceans, their average size is increasing. The biggest transport ship can now carry more than 21.000 containers. These are big, thirsty machines. With [IMO 2020 rules](https://www.reuters.com/article/us-imo-shipping-usa-refiners/new-marine-fuel-rules-to-boost-diesel-prices-for-at-least-a-year-analysts-idUSKCN1TB2CJ) (less sulphur in the fuel) going into effect, more clean fuel will be needed. Right now, ships are burning some of the dirtiest petrol imaginable.

**The world is getting much richer; consumption is rising almost everywhere: A $100tn world GDP** is coming up fast [around 2022?](https://www.statista.com/statistics/268750/global-gross-domestic-product-gdp/) — the world will look very differently once almost everybody consumes the way Westeners do (just think about how much catching up large parts South America, Africa, South East Asia and India still have to do). I believe it is reasonable to assume that several billion additional humans will want to attain a western standard of living: a nice house, a car, several vacations per year and a variety of consumer products from across the globe. By 2050, the UN estimates that there will be just shy of 10 billion humans, the majority of whom will live in cities.

According to the UN, [14% of the world's population does not have access to electricity](https://www.world-nuclear.org/information-library/current-and-future-generation/world-energy-needs-and-nuclear-power.aspx), I expect this number to rapidly decline in the coming decades

A little personal side-note: I recently took an Uber pool in Paris and shared it with a young couple from India, they weren't living in the French capital, they came as tourists (good for them!) - imagine how much more flying will take place once the majority of the world population gets a taste for global tourism.

### The rise of LNG

Liquefied natural gas is compressed natural gas that takes up 600 times less volume. It can be transported by ships for delivery around the world. Before LNG came into widespread use, there had to be a direct pipeline between a natural gas production site and a the location of its usage (direct delivery to homes for heating and cooking or to a gas turbine to produce electricity). With large LNG tankers, it becomes feasible for natural gas from Qatar to be used in Chinese homes, or Australian gas to be used to produce electricity in Pakistan. As more LNG terminals are being built, gas from Texas will be used in Japan. [In 2018 around 314 million tons of LNG have been sold, a yearly growth rate of ~8%.](https://www.forbes.com/sites/judeclemente/2019/07/12/9-things-to-know-about-the-booming-global-liquefied-natural-gas-market/#c85effd4d393)

### The demise of coal

[More than a quarter](https://www.bp.com/content/dam/bp/business-sites/en/global/corporate/pdfs/energy-economics/statistical-review/bp-stats-review-2019-full-report.pdf) of the world's electricity needs are still met through the burning of coal. As the dirtiest-burning fossil fuel (in terms of CO2-emissions), lots of political pressure makes coal less economical (fewer subsidies, protesters hindering construction projects). Air pollution is so bad in many areas of the world, especially in metropolitan agglomerations that coal plants are being turned off. Often to be replaced by cleaner burning natural gas, especially in [China there is strong policy support for coal to gas switching](https://www.shell.com/promos/overview-shell-lng-2019/_jcr_content.stream/1551087443922/1f9dc66cfc0e3083b3fe3d07864b2d0703a25fc4/lng-outlook-feb25.pdf).

### Global warming, cold US-winter

Earth's weather is an extremely complex system: Change one variable, such as the amount of ice in the Arctic, and plenty of unexpected things might start happening. The [polar vortex reaching further south](https://www.carbonbrief.org/qa-how-is-arctic-warming-linked-to-polar-vortext-other-extreme-weather) is one of them. While the temperatures are rising globally, some exceptionally long and cold winters have been recorded in the Northern hemisphere. For more detail, please consult [this article from National Geographic](https://www.nationalgeographic.com/environment/2019/01/climate-change-colder-winters-global-warming-polar-vortex/).
 
Especially in the United States, where [many homes are poorly insulated](https://www.prnewswire.com/news-releases/ninety-percent-of-us-homes-are-under-insulated-300151277.html), cold weather directly translates to higher gas energy consumption. Around 50% of American homes are heated using natural gas. An inverse effect can also be observed: through global warming, summers are getting a lot hotter in many densely populated areas, leading people to direct more energy towards cooling, once again increasing natural gas demand and making global warming worse. 

### The decline of Nuclear Energy

In principle, nuclear energy should be a powerful force in fighting climate change. There are no CO2 emissions, there is no air pollution and electricity can be produced cheaply and reliably. The questions of what to do with nuclear waste and how to minimize the probability of a meltdown (3-mile Island, Chernobyl and Fukushima are not forgotten) prevent the large funding to make this technology part of the future. There is a global trend to phase out nuclear energy, you can find [an exhaustive overview here](https://en.wikipedia.org/wiki/Nuclear_power_phase-out).

[Fusion is promising](https://www.youtube.com/watch?v=mZsaaturR6E) but it always seems to be decades away from production use. There are promising technologies under development, but it remains an open question whether political obstacles and technological hurdles can be overcome. 

For the current state of nuclear technology see here: [What is the future of nuclear energy?](https://www.iaea.org/newscenter/multimedia/videos/what-is-the-future-of-nuclear-energy) and Bill Gates' [Terrapower](https://terrapower.com/).

![German anti nuclear protesters](https://www.tagesspiegel.de/images/biblis2010/3965142/3-format140.jpg)

### low interest rates

In order to default, oil majors would have to face difficulties serving their debt. The last 10 years have seen some of the lowest interest rates and the rise of Quantitative Easing (central banks purchasing bonds in the open market in an effort to decrease interest rates and stimulate economic activity). While absolute debt loads are pretty high, the interest rate that firms have to pay is at record lows. Most large companies can easily serve bond-holders even with limited profitability. As an example, here’s an [overview of Shell’s debt maturities](https://www.shell.com/investors/financial-reporting/debt-information/bonds-and-credit-ratings.html). They have been able to borrow **$1.2bn for an interest rate of 0.375%**! As old bonds are retired, companies are getting more credit-worthy, because they refinance at much lower rates.

### Load balancing

As renewable energy in the form of wind and solar becomes more prevalent, natural gas becomes a lot more attractive. While coal and nuclear plants take a long time to power up and down, natural gas is excellent for load-balancing (to quickly step in when there is not enough sunlight or wind), energy transportation and storage. Energy storage becomes important whenever there is a mismatch between the amount of electricity produced and consumed: on a sunny summer day, solar panels might produce more electricity than is used in their immediate vicinity and, conversely, there's sometimes not enough wind to cover the energy demand. 

Renewable energy such as solar and wind power can expect a large boost if battery and energy transportation technologies come to fruition.

### Supply

Supply is more tricky to think about, Iran is a big producers but currently being sanctioned by the US (there are some export waivers though), Venezuela is imploding and one result of the turmoil is tumbling oil production. It's tough to predict how the political situation in the middle east (where lots of oil producers are) will develop. Will there be a violent conflict between Iran and Saudi-Arabia? Will Iraq be able to peacefully produce oil?

Oil reserves are not all created equal: Some more much more difficult to access, because they are at the bottom of the sea or in the middle of a warzone, factors like that drive production costs. If it costs $60 a barrel to extract oil and the price of oil is $40, no rational capitalist actor will drill for this oil for very long.

### oil as a finite resource

How much oil is there in the world? This question is difficult to answer, there have been many estimations but they've often proven to be wrong. It's been a long time that "peak oil" has been on everybody's mind. Plenty of commentators have claimed in the past that we would have already run out of oil at this point. Vast new discoveries of oil in many regions (off the cost of Brazil, around Cyprus and in the south-china sea) and vast technological progress (Fracking and deep water drilling) have made it possible to make more and more oil recoverable. The current estimate 

It's important to understand why it is so difficult to get a proper grasp on oil reserves, countries like Saudi Arabia are borrowing on the international financial markets by issuing bonds. The vast majority of the Saudi Economy is based on the oil industry, crude is its biggest export. It's in the interest of the ruling family to let the world think that they have very large reserves, because this decreases the risk of default and makes borrowing cheaper. The fate of Saudi Arabia is closely tied to the oil market.

### Geopolitical tensions

many of the biggest oil fields in the world are located in between Iran and Saudi Arabia, two countries that have engaged in plenty of sabre-rattling and proxy wars throughout the region. Nobody knows whether violent conflict will be avoided in the future.

### The US shale boom

Natural gas prices have been falling for years and are currently at a decade low in the United States. I presume that this will lead to a large build-up of investment in to gas turbines and LNG terminals to enable cheap and dependable electricity consumption. As more and more consumption takes place and less investment into new production infrastructure is made, rising prices should be expected. Some industry observers [forecast a supply shortfall around 2023](https://www.iea.org/wei2018/). The low gas prices hurt right now (especially for smaller firms that only make money through upstream activities), but integrated majors such as Total, Shell and Chevron can withstand these challenges.

### creating natural gas from coal

### the price of carbon

Several governments have introduced carbon-pricing, which forces companies to find lower carbon ways of producing natural gas and oil.

### in conclusion

I hope that humanity will be able to free itself from its addiction of fossil fuels (arguably it is the primary reason for violent conflict in the last 30 years and the driving force behind climate change), but my bet is that things will get worse for a lot longer before we even reach the inflection point of “peak oil”.

Change must happen. Change will happen. I don't think the oil and gas industry is anywhere near to disappearing. Given the current valuation of many integrated oil majors, the favorable growth and interest rate environment, I believe that it is a good time to invest in oil companies.
